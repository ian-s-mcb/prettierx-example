# prettierx-example

A tiny example of a JS app with the [StandardJS][standard] linter and
the [PrettierX][prettierx] formatter. The linter and formatter are set
up with [ESLint][eslint] and the [ESLint plugin for PrettierX][plugin].

This setup works well the Vim plugin [ALE][ale] using the .vimrc config:

```vimscript
let g:ale_fixers = {'javascript': ['eslint']}
```

I chose this setup after being dissatisfied with
[prettier-standard][alt-tool] and the lack of visual feedback in ALE for
PrettierX format issues (as demonstrated in [this other example
repo][other-example]).

[ale]: https://github.com/dense-analysis/ale
[alt-tool]: https://github.com/sheerun/prettier-standard
[eslint]: https://eslint.org/
[plugin]: https://github.com/aMarCruz/eslint-plugin-prettierx
[other-example]: https://gitlab.com/ian-s-mcb/prettier-standard-example
[prettierx]:https://github.com/brodybits/prettierx
[standard]: https://github.com/standard/standard
